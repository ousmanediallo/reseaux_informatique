# Reseaux_Informatique

Ce dépôt contiennent mes projets et travaux pratiques de réseau informatique.

### TPreseaux : [TPresaux](TPresaux)
 - juste des modifs apporter au code.

### Projet_reseau_01 : [projet_reseau_01](projet_reseau_01)
 - Des warning qui sont dû au fait du casting des types de donnée au niveau de la création du thread dans le main() mais qui n'empêche pas la compilation et l'exécution normale avec : "gcc -Wall ping3-s.c net_aux.c -o ping3-s -lpthread".

### Projet_reseau_02 : [projet_reseau_02](projet_reseau_02)
 - J'ai des warnings qui sont dû au fait du casting des types de donnée au niveau de la création des thread mais n'empêche pas la compilation normale  avec : 
    - "gcc -Wall node.c net_aux.c -o node-c -lpthread" 
    - "./node-c -f file1.txt" 
    - Le -f : désigne le fichier d'initialisation de la liste de FILM.
    - L'adresse du serveur : 127.0.0.1 et le port : 8000

