/* déclaration d'une variable de type attributs de thread  */
pthread_attr_t attr;

/* initialisation de la variable attr */
ret = pthread_attr_init(&attr); 
if (ret!=0){
    perror("Erreur dans la création des attributs du thread");
    exit(EXIT_FAILURE);
}
/* modification des attributs ici (DETACHED) */
ret = pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
if (ret!=0){
    perror("Erreur dans la mise en place des attribus du thread");
    exit(EXIT_FAILURE);
}
 
