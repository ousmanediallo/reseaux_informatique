#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include "net_aux.h"

#define BUFFERMAX 128

void proc_client(int);

int main(int argc, char** argv){
    int   sock;
    int   sock_effective;
    int   port = 8000;
    char *ip_serveur="0.0.0.0"; 
    pid_t pid_fils;

    /* Lecture des paramettres en ligne de commande */
    if(argc > 1){
		int i=1;
		char *option;
		
		while (i<argc) {
			option=argv[i++];
			if (strcmp(option,"-h")==0) {
				fprintf(stderr, "Utilisation : %s [-h] [-a ip serveur] [-p port serveur] \n", argv[0]);
				fprintf(stderr, "     -p : port du serveur\n");
				fprintf(stderr, "     -h : cette aide \n");
				exit (EXIT_SUCCESS);

			} else if  (strcmp(option,"-p")==0) { 	
				if (i==argc){
					fprintf(stderr, "il manque le port du serveur\n");
					exit(EXIT_FAILURE);
				}
				port = atoi(argv[i++]);

			} else {
				fprintf(stderr, "Option non roconue essayez -h \n");
				exit(EXIT_FAILURE);
			}
		}
    }
    
    /* creation d'une socket */
    sock = create_socket();

    /* Démmarage d'un serveur */
    start_server(sock, ip_serveur, port);
    
    while(1){
	
		/* attendre une connection */
		sock_effective = wait_connection(sock);
		
		/* on crée un processus fils */ 
		pid_fils = fork();
		
		/* une erreur de fork */
		if(pid_fils == -1){ 
			fprintf(stderr, "(%d) Erreur de création de processus\n", 
				getpid());
			exit(EXIT_FAILURE);
		}
		
		/* partie du processus fils */
		else if(pid_fils == 0){ 
			proc_client(sock_effective);
		}
		
		/* la partie du processus parent */
		else if(pid_fils > 0) { 
			fprintf(stderr,"(%d) Processus %d créé \n",getpid(), pid_fils);
			
			/* on ferme la socket client, le fils en a une copie */
			close_connection(sock_effective);
		}
    }
    
    
    close_connection(sock);

    return EXIT_SUCCESS;
}

void proc_client(int s){
    char buf[BUFFERMAX];
    int  en_communication = 1;
		
    while(en_communication){
	
		/* lire un message */
		sock_receive(s, buf, BUFFERMAX);
		sleep(1);
		if(message_is(buf, "ping")) {
			sock_send(s, "pong");
		}	    
		else if(message_is(buf, "quit")) {
			en_communication = 0;
		}
		else	{
			fprintf(stderr, "(%d) Message inattendu\n",
				getpid());
			en_communication = 0;
		}
    } 
    
    /* fermer la communication */
    usleep(2000);
    close_connection(s);
    exit(EXIT_SUCCESS);
}

