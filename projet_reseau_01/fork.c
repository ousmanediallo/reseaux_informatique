#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

int main(int argc, char **argv){
   
   pid_t pid_fils;
  
   pid_fils = fork();

   if ( pid_fils == -1) { 
      fprintf(stderr, "Erreur de création du nouveau processus\n"); 
      exit(1);
   }
   if ( pid_fils == 0 ) {                                 /* <-- cette partie sera executée */
      printf("Fils: mon pid %d\n", getpid());             /*     par le fils, jusqu'à la    */
      printf("Fils: le pid de mon père %d\n", getppid()); /*     fin du if                  */ 
      /* faire des choses ... */
   }
   else {                                                 /* <-- cette partie sera executée */ 
      printf("Pere: mon pid %d\n", getpid());             /*     par le père, jusqu'à la    */  
      printf("Pere: le pid de mon père %d\n", getppid()); /*     fin du else                */ 
      printf("Pere: Le pid de mon fils %d\n", pid_fils);
      /* faire des choses */
  }
   
  sleep(1);
  printf("Processus %d termine\n", getpid());             /* <-- cette partie sera executée */
                                                          /*     par les deux processus,    */ 
  return 0;                                               /*     jusqu'à la fin du programe */
}
