#define _REENTRANT
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h> // le pthread
#include "net_aux.h"

#define BUFFERMAX 128
#define NUM_THREAD 5

/***un thread du ping***/
void* thread_s(void *sock) {
    char buf[BUFFERMAX];
    long s;
    s = (long) sock; /***récupération du paramètre socket (cast vers le type long)***/
    int pas_fini = 1;

    while(pas_fini){
        sock_receive(s, buf, BUFFERMAX);

        if(strncmp(buf, "ping", strlen("ping")) == 0 ){
            sock_send(s, "pong");
        }
        else{
            usleep(2000);
            close_connection(s);
            pas_fini = 0;
        }
    }
    /***fin du thread du ping***/
    pthread_exit(NULL); 
}

int main(int argc, char** argv){

    int sock;
    int  sock_effective;
    int port = 8000;
    char *ip_serveur="0.0.0.0";
    pthread_t thrd[NUM_THREAD];
    int ret, k=0;

    /***Lecture des paramettres en ligne de commande***/
    if(argc > 1){
        int i=1;
        char *option;

        while (i<argc) {
            option=argv[i++];

            if (strcmp(option,"-h")==0) {
                fprintf(stderr, "Utilisation : %s [-h] [-a ip serveur] [-p port serveur] \n", argv[0]);
                fprintf(stderr, "     -p : port du serveur\n");
                fprintf(stderr, "     -h : cette aide \n");
                exit (EXIT_SUCCESS);

            } else if  (strcmp(option,"-p")==0) {
                if (i==argc) {
                    fprintf(stderr, "il manque le port du serveur\n");
                    exit(EXIT_FAILURE);
                }
                port = atoi(argv[i++]);

            } else {
                fprintf(stderr, "Option non roconue essayez -h \n");
                exit(EXIT_FAILURE);
            }
        }
    }

    /***creation d'une socket***/
    sock = create_socket();

    /***Démmarage d'un serveur***/
    start_server(sock, ip_serveur, port);

    while(1) {

        /***attendre la requete de connexion***/
        sock_effective = wait_connection(sock);

        /***creation du thread***/
        ret = pthread_create(&thrd[k++], NULL, thread_s, (void*)(sock_effective));
        if (ret!=0) {
            printf("Erreur dans la création du thread\n");
            pthread_exit(NULL);
        }

        sleep(1);
    }

    /***fin de la thread principal main***/
    pthread_exit(NULL); 
    close_connection(sock);

    return EXIT_SUCCESS;
}
