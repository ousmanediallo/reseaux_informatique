#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "net_aux.h"

#define BUFFERMAX 100

int main(int argc, char** argv){
	
  
    int                sock;            
    char               buf[BUFFERMAX];  
    char               ip_serveur[BUFFERMAX]; 
    int                port;            
    int                num_ping,i;
    int                pas_fini;
   
    /* on fixe les valeurs par defaut  */
    port = 8000;
    strcpy(ip_serveur, "127.0.0.1");
    num_ping=5;
    
    /* Lecture des paramettres en ligne de commande */
    if(argc > 1){
	int i=1;
	char *option;
	
	while (i<argc) {
	    option=argv[i++];
	    
	    if (strcmp(option,"-h")==0) {
		fprintf(stderr, "Utilisation : %s [-h] [-a ip serveur] [-p port serveur] [-n num ping] \n", argv[0]);
		fprintf(stderr, "     -a : ip du serveur \n");
		fprintf(stderr, "     -p : port du serveur\n");
		fprintf(stderr, "     -n : nombre de ping a envoyer ( 0 pour infini) \n");
		fprintf(stderr, "     -h : cette aide \n");
		exit (EXIT_SUCCESS);

	    } else if  (strcmp(option,"-p")==0) { 	
		if (i==argc){
		    fprintf(stderr, "il manque le port du serveur\n");
		    exit(EXIT_FAILURE);
		}
		port = atoi(argv[i++]);

	    } else if  (strcmp(option,"-a")==0) {	
		if (i==argc){
		    fprintf(stderr, "il manque l'ip du serveur \n");
		    exit(EXIT_FAILURE);
		}
		strncpy(ip_serveur, argv[i++], BUFFERMAX);

	    } else if  (strcmp(option,"-n")==0) {	
		if (i==argc){
		    fprintf(stderr, "il manque le nombre de ping \n");
		    exit(EXIT_FAILURE);
		}
		num_ping = atoi(argv[i++]);
	    } 

	    else {
		fprintf(stderr, "Option non roconue essayez -h \n");
		exit(EXIT_FAILURE);
	    }
	}
    }

    /* création de la socket de communication */
    sock = create_socket();
    
    /* on demande une connexion au serveur*/
    open_connection(sock, ip_serveur, port);    


    /* on envoi le premier ping au serveur */

    sock_send(sock, "ping");

 
    i=num_ping;

    pas_fini = 1;
    while( pas_fini ){ 
	/* condition d'arret */
	i--;
	pas_fini = (i!=0);

	/* on lit le message du serveur */
	sock_receive(sock, buf,BUFFERMAX); 
	
	/* si c'est un pong en onvoit le ping */
	if( strncmp(buf, "pong", strlen("pong")) == 0 ){
	    sleep(1);
	    sock_send(sock, "ping");
	}


	/* si c'est autre chose on envoit quit */
	else {
	    pas_fini=0;
	}
    }
    sleep(3);
    /* on dit au serveur que nous avons fini en envoyant un quit */
    sock_send(sock, "quit");

    /* On ferme la socket d'ecoute de connexion */
    close_connection(sock);
    
    return EXIT_SUCCESS;
}
