/* Cette constante permet d'utiliser les versions "thread safe" des */
/* fonction de la lib C elle est OBLIGATOIRE */ 
#define _REENTRANT

#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

#define NUM_THREADS     5

/* un thread */
void* thread_a(void *thread_id){
   long t_id;
   t_id=(long)thread_id; /* récupération du paramètre (cast vers le type long)*/
   printf("Thread A : je suis le thread numéro %ld\n", t_id);
   pthread_exit(NULL); /* fin du thread */
}

/* un second thread */
void* thread_b(void *thread_id){
   long t_id;
   t_id=(long)thread_id; /* récupération du paramètre (cast vers le type long)*/
   printf("Thread B : je suis le thread numéro %ld\n", t_id);
   pthread_exit(NULL); /* fin du thread */
}

/* le thread principal main */
int main (int argc, char *argv[]){
    pthread_t    thrd[NUM_THREADS*2]; /* déclaration d'un tableau de threads */
    int          ret;
    long         i;

    /* création de 5 threads A */
    for(i=0; i<NUM_THREADS; i++){
        printf("Thread main : création du thread numéro %ld\n", i);
        /* création du thread  */
        ret = pthread_create(&thrd[i], NULL, thread_a, (void *)i);
        if (ret!=0){
            printf("Erreur dans la création du thread\n");
            pthread_exit(NULL);
        }
    }
    /* création de 5 threads B */
    for(i=NUM_THREADS; i<NUM_THREADS*2; i++){
        printf("Thread main : création du thread numéro %ld\n", i);
        /* création du thread  */
        ret = pthread_create(&thrd[i], NULL, thread_b, (void *)i);
        if (ret!=0){
            printf("Erreur dans la création du thread\n");
            pthread_exit(NULL);
        }
    }
    pthread_exit(NULL);  /* fin du thread principal main */
}
