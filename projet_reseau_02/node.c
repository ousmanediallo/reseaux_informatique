#define _REENTRANT
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <ctype.h>
#include "net_aux.h"

#define BUFFERMAX 128
#define NUM_THREAD 15
#define FILM_SIZE 70
#define FILM_STRING 40
#define FILM_SIZE_SIG "nombre_film"
#define FILM_COPY_SIG "film_copier"

int taille_film = 0;
char TAB_FILM[FILM_SIZE][FILM_STRING]; // tableau à deux dimensions contenant la taille et les chaines de caractere 

int numero(char *value){
    int i = 0;
    for(i=0; i<strlen(value)-1; i++){
        if(!isdigit(value[i])){ // isdigit : teste si un caractère est un chiffre ou non
            return 0;
        }
    }
    return 1;
}

// lecture du fichier des FILM pour l'instance de ce serveur
void getFilmList(char *nom_fichier){
    FILE *file;
    char *buff = NULL;
    size_t length = 0;

    file = fopen(nom_fichier, "r"); // lire le contenu du fichier 
    if(file==NULL){
        fprintf(stderr, "Erreur de récupération des FILM dans le fichier=%s", nom_fichier);
        exit(EXIT_FAILURE);
    }
    int i = taille_film;
    
    while(i<FILM_SIZE && getline(&buff, &length, file)!=-1){
        strncpy(TAB_FILM[i++], buff, strlen(buff)-1); // copier le contenu d'une chaîne de caractère
    }
    taille_film = i;
    fclose(file);
    if(buff) { free(buff); }
}

// permet de verifer qi la chaine de caractere exist dans le tableau en faisant une comparaison au second 
int existDeja(char *value){
    int i = 0;
    for(i=0; i<taille_film; i++){
        if(strcmp(value, TAB_FILM[i])==0){
            return 1;       
        }
    }
    return 0;
}

// permet de recuperer le nombre de film  
int getNbrFilm(int sock_client){
    char buf[BUFFERMAX];
    
    sock_send(sock_client, FILM_SIZE_SIG);
    sleep(1);
    sock_receive(sock_client, buf, BUFFERMAX);
    int i = 0;

    if(numero(buf)){
     	i = atoi(buf);
        return i;
    }
    return 0;
}


// un thread pour quand le serveur se comporte comme un client pour récupérer les FILM chez un autre
void* thread_c(void *sock){
    char buf[BUFFERMAX];
    int port, i, pas_fini, nombre_film = 0; // port du serveur à contacter
    char ip_serveur[BUFFERMAX];
    
    while(1){
		// demander l'adresse ip et le numero du port 
		fprintf(stdout, "ENTRER IP SERVER A CONTACTER : ");
		fscanf(stdin, " %s", ip_serveur);
		
        fprintf(stdout, "ENTRER LE PORT DU SERVER A CONTACTER : ");
        fscanf(stdin, " %d", &port);

        // lancer le client : création de la socket de communication
        sock = create_socket();

        // solliciter connexion au serveur
        open_connection(sock, ip_serveur, port);

        // demande du nombre de FILM
        nombre_film = getNbrFilm(sock);
        
        if(nombre_film!=0){
			fprintf(stderr, " C: RECUPERER %d FILM DU (%s:%d).\n", nombre_film, ip_serveur, port);
            // Debut de la récupération des n FILM
            fprintf(stderr, " C: DEMMARER LA COPIE DES FILMS.\n");
            for(i=0; i<nombre_film; i++){
                sock_send(sock, FILM_COPY_SIG);
                sleep(1);
                sock_receive(sock, buf, BUFFERMAX);
                if(existDeja(buf)){
                    fprintf(stderr, " C: COPIE ANNULER DE \"%s\" IL EXISTE DEJA.\n", buf);
                    continue;
                } 
                strcpy(TAB_FILM[taille_film++], buf);
                fprintf(stderr, " C:COPIE TERMINER \"%s\" AVEC SUCCES.\n", buf);
            }
            fprintf(stderr, " C:  CONTIENT %d FILM ACTUELLE.\n", taille_film);
		}else{
			fprintf(stderr, " C: PAS DE FILM A RECUPERER \n");
		}

        sleep(1);
        // on dit au serveur que nous avons fini en envoyant un quit
        sock_send(sock, "quit");

        // On ferme la socket d'ecoute de connexion
        close_connection(sock);
    }


    pthread_exit(NULL); // fin du thread du ping
}

// un thread du ping pour le serveur
void* thread_s(void *sock){
    char buf[BUFFERMAX];
    int s;
    s = (int)(sock); // récupération du paramètre socket "cast vers le type int"

    int pas_fini = 1, i = 0;
    
    while(pas_fini){
        sock_receive(s, buf, BUFFERMAX);

        if(strncmp(buf, FILM_SIZE_SIG, strlen(FILM_SIZE_SIG)) == 0 ){
			
			fprintf(stderr, " S: ON POSSEDE %d FILM.\n", taille_film);
			snprintf(buf, 10, "%d", taille_film);
            sock_send(s, buf);
        } else if(strncmp(buf, FILM_COPY_SIG, strlen(FILM_COPY_SIG))==0){
            fprintf(stderr, " S: ENVOI DE \"%s\" EN COURS...\n", TAB_FILM[i]);
            strncpy(buf, TAB_FILM[i++], BUFFERMAX);
            sock_send(s, buf);
        } else {
            usleep(2000);
            close_connection(s);
            pas_fini = 0;
        }
    }
    pthread_exit(NULL); // fin du thread du ping
}


int main(int argc, char** argv){

    int sock;
    int sock_effective, sock_client;
    int port = 8000;
    char *ip_serveur="127.0.0.1";
    pthread_t thrd[NUM_THREAD];
    int ret, k=0;
    char *nom_fichier = NULL;

    // lecture des paramettres en ligne de commande
    if(argc > 1){
	int i=1;
	char *option;

        while (i<argc) {
            option=argv[i++]; //argv : recupere le contenu du fichier quand est sup à 1

            if (strcmp(option,"-h")==0) {
            fprintf(stderr, "Utilisation : %s [-h] [-a ip serveur] [-p port serveur] \n", argv[0]);
            fprintf(stderr, "     -p : port du serveur\n");
            fprintf(stderr, "     -f : fichier d'initialisation de la liste de FILM"); //initialisation du fichier de lecture 
            fprintf(stderr, "     -h : cette aide \n");
            exit (EXIT_SUCCESS);

            } else if  (strcmp(option,"-p") == 0) {
                if (i==argc){
                    fprintf(stderr, "MANQUE DU PORT DU SERVER \n");
                    exit(EXIT_FAILURE);
                }

                port = atoi(argv[i++]);

            } else if (strcmp(option, "-f") == 0){
                if(i==argc){
                    fprintf(stderr, "MANQUE DU NOM FICHIER FILM\n");
                    exit(EXIT_FAILURE);
                }
                nom_fichier = argv[i++]; //trasmet le contenu dans le pointeur nom_fichier
            }
            else {
                fprintf(stderr, "Option non reconnue essayez -h \n");
                exit(EXIT_FAILURE);
            }
        }
    }

    sock = create_socket();

    start_server(sock, ip_serveur, port);

    // Initialisation de la liste de FILM
    if(nom_fichier!=NULL){
        getFilmList(nom_fichier);
    }

    while(1){
		
		// Creation d'un nouveau thread pour connecter ce serveur en tant que client aux autres serveur
		ret = pthread_create(&thrd[k++], NULL, thread_c, (void *)(sock_client));

        if (ret!=0){
            printf("Erreur dans la création du thread\n");
            pthread_exit(NULL);
        }

        // attendre la requete de connexion
        sock_effective = wait_connection(sock);

        // creation du thread
        ret = pthread_create(&thrd[k++], NULL, thread_s, (void *)(sock_effective));

        if(ret!=0){
            printf("Erreur dans la création du thread\n");
            pthread_exit(NULL);
        }

        sleep(1);
    }

    pthread_exit(NULL);
    close_connection(sock);

    return EXIT_SUCCESS;
}

